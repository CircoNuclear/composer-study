<?php

namespace Luiz\Solid;

class HtmlTest extends \PHPUnit\Framework\TestCase
{

    public function    testSimplesDeExemplificacaoDeTeste()
    {

        $html = new Html;
        $img = $html->img('images/photo.jpg');
        $this->assertEquals("<img src='images/photo.jpg'>", $img);

    }

    public function    testCriaUmLinkComImagemComoAncora()
    {

        $html = new Html;
        $img = $html->img('images/photo.jpg');
        $link = $html->a('http://seusite.com/perfil', $img);
        $this->assertEquals("<a href='http://seusite.com/perfil'><img src='images/photo.jpg'></a>", $link);

    }

    public function  testCriaListaNaoOrdenadaComConteudo()
    {

        $html = new Html;
        $ul = $html->ul('<li>teste</li>');
        $this->assertEquals("<ul><li>teste</li></ul>", $ul);

    }

}